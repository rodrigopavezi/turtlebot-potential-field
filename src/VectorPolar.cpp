/*
 * VectorPolar.cpp
 *
 *  Created on: Mar 10, 2013
 *      Author: rodrigopavezi
 */

#include "VectorPolar.h"

VectorPolar::VectorPolar(double distance, double angle) {
	m_distance = distance;
	m_angle	= angle;
}

void VectorPolar::setDistance(double distance) {
	m_distance = distance;
}

void VectorPolar::setAngle(double angle) {
	m_angle = angle;
}


