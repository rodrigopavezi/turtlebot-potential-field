/*
 * VectorPolar.h
 *
 *  Created on: Mar 10, 2013
 *      Author: rodrigopavezi
 */

#ifndef VECTORPOLAR_H_
#define VECTORPOLAR_H_

class VectorPolar {
private:
	double m_distance;
	double m_angle;
public:
	VectorPolar() {}
	VectorPolar(double distance, double angle);

	void setDistance(double distance);
	void setAngle(double angle);
	double getDistance() { return m_distance; }
	double getAngle()	{ return m_angle; }
};

#endif /* VECTORPOLAR_H_ */
